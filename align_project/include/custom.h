#pragma once

#include <string>
#include <iostream>
#include "matrix.h"
#include "io.h"
#include "mathmac.h"
#include "reflect_across_edge.h"

Image custom(const Image &src, const Matrix<double> &kernel);
std::tuple<uint, uint, uint> convol(const Image &img, const Matrix<double> &kernel);
