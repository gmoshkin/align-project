#pragma once

#include <iostream>
#include "mathmac.h"
#include "matrix.h"
#include "io.h"
#include "canny.h"
#include "resize.h"
#include "gray_world.h"
#include "unsharp.h"
#include "autocontrast.h"

Image align(const Image &src, const std::string &postprocessing = "", double fraction = .0);
Image cut_edge(const Image &src, const Image &edge);
uint get_new_edge_up(const Image &edge);
uint get_new_edge_down(const Image &edge);
uint get_new_edge_left(const Image &edge);
uint get_new_edge_right(const Image &edge);
std::tuple<int, int> pyramid(const Image &, const Image &, std::tuple<int, int>, double &, double);
std::tuple<int, int> best_shift(const Image &s, const Image &r, std::tuple<int, int> &, double);
// double metric(const Image &img1, const Image &img2);
double metric(const Image &img1, const Image &img2, int row1, int col1, int row2, int col2, int h, int w);
// double standart_deviation(const Image &img1, const Image &img2);
double standart_deviation(const Image &img1, const Image &img2, int row1, int col1, int row2, int col2, int h, int w);
// double cross_correlation(const Image &img1, const Image &img2);
double cross_correlation(const Image &img1, const Image &img2, int row1, int col1, int row2, int col2, int h, int w);
Image merge_red_green(const Image &red, const Image &green, int i0, int j0);
Image merge_red_green_blue(const Image &rg, const Image &blue, int i0, int j0);
