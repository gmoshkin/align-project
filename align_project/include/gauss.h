#pragma once

#include <string>
#include <iostream>
#include <math.h>
#include "matrix.h"
#include "io.h"
#include "mathmac.h"
#include "reflect_across_edge.h"
#include "custom.h"

Image gaussian(const Image &src, double sig, uint rad);
Image gaussian_separable(const Image &src, double sig, uint rad);
