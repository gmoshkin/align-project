#pragma once

#include <math.h>
#include <string>
#include "gauss.h"
#include "io.h"
#include "mathmac.h"
#include "matrix.h"
#include "sobel.h"
#include "reflect_across_edge.h"

Image canny(const Image &src, int threshold1, int threshold2);
Image gray_scale(const Image &img);
void gradient(const Image &img, Matrix<double> &abs, Matrix<double> &arg);
int convol(const Image &img, int row, int col, const Matrix<double> &kernel);
int grad_x(const Image &img);
int grad_y(const Image &img);
Image edge(Matrix<double> &grad_abs, Matrix<double> &grad_arg, int t1, int t2);
// int check_edge_pixel(const Matrix<int> &abs_vicinity, double arg, int t1, int t2);
int check_edge_pixel(const Matrix<double> &grad_abs, int row, int col, double arg, int t1, int t2);
// void get_neighbours(const Matrix<int> &vic, double arg, double &f, double &b);
void get_neighbours(const Matrix<double> &grad_abs, int row, int col, double arg, double &front, double &back);
void check_neighbourhood(Matrix<int> &map, Image &res, uint row, uint col);
