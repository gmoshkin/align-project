#pragma once

#include <iostream>
#include <string>
#include "mathmac.h"
#include "matrix.h"
#include "io.h"
#include "reflect_across_edge.h"

class UnsharpOp
{
public:
	static const uint radius;
	static const uint size;

	static const Matrix<double> kernel;

	std::tuple<uint, uint, uint> operator () (const Image &img) const;
};

Image unsharp(const Image &src);
