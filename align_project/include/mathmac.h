#pragma once

// Mathematical macros

#define ABS(x) (((x) > 0) ? (x) : (-(x)))
#define SQR(x) ((x) * (x))
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
