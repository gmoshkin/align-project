#pragma once

#include <iostream>
#include <strings.h>
#include "mathmac.h"
#include "matrix.h"
#include "io.h"

Image autocontrast(const Image &src, double fraction);
Matrix<uint> get_y(const Image &img);
Image adjust(const Image &img, int min, int max);
