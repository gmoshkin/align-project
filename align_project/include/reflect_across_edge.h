#pragma once

#include <iostream>
#include <string>
#include "matrix.h"
#include "io.h"

Image reflect_across_edge(const Image &src, int radh, int radw = -1);
