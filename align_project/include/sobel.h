#pragma once

#include <iostream>
#include "matrix.h"
#include "io.h"
#include "mathmac.h"
#include "custom.h"

extern const Matrix<double> kernel_x;
extern const Matrix<double> kernel_y;

Image sobel_x(const Image &src);
Image sobel_y(const Image &src);
