#pragma once

#include <iostream>
#include <string>
#include <cassert>
#include "mathmac.h"
#include "matrix.h"
#include "io.h"

Image resize(const Image &src, double scale);
