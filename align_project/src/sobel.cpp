#include "sobel.h"

using std::string;

const Matrix<double> kernel_x = {{ -1,  0,  1 },
								 { -2,  0,  2 },
								 { -1,  0,  1 }};
const Matrix<double> kernel_y = {{  1,  2,  1 },
								 {  0,  0,  0 },
								 { -1, -2, -1 }};

Image sobel_x(const Image &src)
{
	if (3 > src.n_rows || 3 > src.n_cols)
		throw string("the kernel is bigger than the image");

	return custom(src, kernel_x);
}

Image sobel_y(const Image &src)
{
	if (3 > src.n_rows || 3 > src.n_cols)
		throw string("the kernel is bigger than the image");

	return custom(src, kernel_y);
}
