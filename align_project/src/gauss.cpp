#include "gauss.h"

using std::cout;
using std::endl;

using std::string;
using std::tuple;
using std::get;
using std::tie;
using std::make_tuple;

const double pi = 3.14159265358979323;

Image gaussian(const Image &src, double sigma, uint radius)
{
	uint size = 2 * radius + 1;

	if (size > src.n_rows || size > src.n_cols)
		throw string("the kernel is bigger than the image");

	Matrix<double> kernel(size, size);
	double norm = 0;

	for (uint i = 0; i < size; ++i) {
		for (uint j = 0; j < size; ++j) {
			norm += kernel(i, j) = exp(-0.5 * (SQR(i - radius) + SQR(j - radius)) / SQR(sigma));
		}
	}
	for (uint i = 0; i < size; ++i) {
		for (uint j = 0; j < size; ++j) {
			kernel(i, j) /= norm;
		}
	}

	return custom(src, kernel);
}

Image gaussian_separable(const Image &src, double sigma, uint radius)
{
	uint size = 2 * radius + 1;

	if (size > src.n_rows || size > src.n_cols)
		throw string("the kernel is bigger than the image");

	Matrix<double> kernel_h(1, size), kernel_v(size, 1);
	double norm = 0;

	for (uint i = 0; i < size; ++i) {
		norm += kernel_h(0, i) = exp((-0.5 * SQR(i - radius)) / SQR(sigma));
	}
	for (uint i = 0; i < size; ++i) {
		kernel_v(i, 0) = kernel_h(0, i) /= norm;
	}

	return custom(custom(src, kernel_h), kernel_v);
}
