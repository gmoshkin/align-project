#include "custom.h"

using std::cout;
using std::string;
using std::tuple;
using std::tie;
using std::make_tuple;

Image custom(const Image &src, const Matrix<double> &kernel)
{
    const uint kern_h = kernel.n_rows;
    const uint kern_w = kernel.n_cols;
	const uint rad_h = kern_h / 2, rad_w = kern_w / 2;

	Image img = reflect_across_edge(src, rad_h, rad_w);

	const uint n_rows = img.n_rows, n_cols = img.n_cols;

    Image tmp(n_rows, n_cols);

    const uint start_i = rad_h;
    const uint end_i = n_rows - rad_h;
    const uint start_j = rad_w;
    const uint end_j = n_cols - rad_w;

	if (n_rows < kern_h || n_cols < kern_w)
		throw string("kernel is bigger than the image");

    for (uint i = start_i; i < end_i; ++i) {
        for (uint j = start_j; j < end_j; ++j) {
            Image neighbourhood = img.submatrix(i - rad_h, j - rad_w, kern_h, kern_w);
            tmp(i, j) = convol(neighbourhood, kernel);
        }
    }
	return tmp.submatrix(rad_h, rad_w, src.n_rows, src.n_cols);
}

tuple<uint, uint, uint> convol(const Image &img, const Matrix<double> &kernel)
{
	double sum_r = 0, sum_g = 0, sum_b = 0;
	uint n_rows = kernel.n_rows, n_cols = kernel.n_cols;
	uint red, green, blue;

	for (uint i = 0; i < n_rows; ++i) {
		for (uint j = 0; j < n_cols; ++j) {
			tie(red, green, blue) = img(i, j);
			sum_r += kernel(i, j) * red;
			sum_g += kernel(i, j) * green;
			sum_b += kernel(i, j) * blue;
		}
	}
	sum_r = uint(MAX(0, MIN(sum_r, 255)));
	sum_g = uint(MAX(0, MIN(sum_g, 255)));
	sum_b = uint(MAX(0, MIN(sum_b, 255)));
	return make_tuple(sum_r, sum_g, sum_b);
}
