#include "autocontrast.h"

using std::tuple;
using std::make_tuple;
using std::tie;
using std::cout;

Image autocontrast(const Image &src, double fraction)
{
	Matrix<uint> brightness = get_y(src);
	int hist[256];
	uint n_rows = src.n_rows, n_cols = src.n_cols, n_pix = n_rows * n_cols;
	int min = -1, max = 256;
	int n_min = n_pix * fraction, n_max = n_pix * fraction;
	
	memset(hist, 0, 256 * sizeof(int));
	for (uint i = 0; i < n_rows; ++i) {
		for (uint j = 0; j < n_cols; ++j) {
			++(hist[brightness(i, j)]);
		}
	}
	while (!hist[++min]);
	while (n_min) {
		n_min -= MIN(n_min, hist[min]);
		hist[min] -= MIN(n_min, hist[min]);
		min += (hist[min] == 0);
	}
	while (!hist[--max]);
	while (n_max) {
		n_max -= MIN(n_max, hist[max]);
		hist[max] -= MIN(n_max, hist[max]);
		max -= (hist[max] == 0);
	}
	for (int i = 0; i < 256; ++i)
	if (min == max)
		return src;
	return Image(adjust(src, min, max));
}

Matrix<uint> get_y(const Image &img)
{
	uint n_rows = img.n_rows, n_cols = img.n_cols;
	uint red, green, blue;
	Matrix<uint> tmp(n_rows, n_cols);

	for (uint i = 0; i < n_rows; ++i) {
		for (uint j = 0; j < n_cols; ++j) {
			tie(red, green, blue) = img(i, j);
			tmp(i, j) = .2125 * red + .7154 * green + .0721 * blue;
		}
	}
	return tmp;
}

Image adjust(const Image &img, int min, int max)
{
	uint n_rows = img.n_rows, n_cols = img.n_cols;
	int red, green, blue;
	Image tmp(n_rows, n_cols);

	for (uint i = 0; i < n_rows; ++i) {
		for (uint j = 0; j < n_cols; ++j) {
			tie(red, green, blue) = img(i, j);
			red   = MAX(0, MIN(255, (255 * (red   - min) / (max - min))));
			green = MAX(0, MIN(255, (255 * (green - min) / (max - min))));
			blue  = MAX(0, MIN(255, (255 * (blue  - min) / (max - min))));
			tmp(i, j) = make_tuple(red, green, blue);
		}
	}
	return tmp;
}
