#include "resize.h"

using std::tuple;
using std::tie;
using std::make_tuple;
using std::string;
using std::cout;

Image resize(const Image &src, double scale)
{
	int old_rows = src.n_rows;
	int old_cols = src.n_cols;
	
	if (old_rows == 1 || old_cols == 1)
		throw string("interpolation doesn't work if the image is one dimentional");

	int new_rows = floor(old_rows * scale);
	int new_cols = floor(old_cols * scale);

	float tmp, dx, dy;
	int row, col;
	float d1, d2, d3, d4;

	uint r1, r2, r3, r4;
	uint g1, g2, g3, g4;
	uint b1, b2, b3, b4;
	uint red, green, blue;

	Image dst(new_rows, new_cols);
 
	for (int i = 0; i < new_rows; ++i) {
		// find the y coordiate of new image pixel in the old image
		tmp = float(i) * (old_rows - 1) / (new_rows - 1);
		// I can't imagine the situation in which tmp would be negative
		assert(tmp >= 0);
		row = floor(tmp);
		if (row == old_rows - 1)
			row = old_rows - 2;
		// better to keep the shift of the coordinate to decrease the amount of calculations 
		dy = tmp - row;
		for (int j = 0; j < new_cols; ++j) {
			// find the x coordiate of new image pixel in the old image
			tmp = float(j) * (old_cols - 1) / (new_cols - 1);
			// Once again, I can't imagine the situation in which tmp would be negative
			assert(tmp >= 0);
			col = floor(tmp);
			if (col == old_cols - 1)
				col = old_cols - 2;
			// the shift of x coordinate
			dx = tmp - col;

			// calculating the coefficients of interpolation
			d1 = (1 - dx) * (1 - dy);
			d2 = dx * (1 - dy);
			d3 = dx * dy;
			d4 = (1 - dx) * dy;
 
			tie(r1, g1, b1) = src(row, col);
			tie(r2, g2, b2) = src(row, col + 1);
			tie(r3, g3, b3) = src(row + 1, col + 1);
			tie(r4, g4, b4) = src(row + 1, col);
 
			// interpolation takes place here
			red   = r1 * d1 + r2 * d2 + r3 * d3 + r4 * d4;
			green = g1 * d1 + g2 * d2 + g3 * d3 + g4 * d4;
			blue  = b1 * d1 + b2 * d2 + b3 * d3 + b4 * d4;
 
			dst(i, j) = make_tuple(red, green, blue);
		}
	}
	return dst;
}
