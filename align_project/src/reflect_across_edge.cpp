#include "reflect_across_edge.h"

using std::cout;
using std::string;

Image reflect_across_edge(const Image &src, int radh, int radw)
{
	if (radh < 0)
		throw string("radius may not be negative");
	if (radw < 0)
		radw = radh;

	int n_rows_old = src.n_rows, n_cols_old = src.n_cols;
	int n_rows_new = n_rows_old + 2 * radh, n_cols_new = n_cols_old + 2 * radw;
	Image tmp(n_rows_new, n_cols_new);

	// reflecting corners
	for (int i = 0; i < radh; ++i) {
		for (int j = 0; j < radw; ++j) {
			tmp(i, j) = src(radh - i, radw - j);
			tmp(i, n_cols_new - 1 - j) = src(radh - i, n_cols_old - radw - 1 + j);
			tmp(n_rows_new - 1 - i, j) = src(n_rows_old - radh - 1 + i, radw - j);
			tmp(n_rows_new - 1 - i, n_cols_new - 1 - j)
				= src(n_rows_old - radh - 1 + i, n_cols_old - radw - 1 + j);
		}
	}
	// reflecting left and right edges
	for (int i = 0; i < n_rows_old; ++i) {
		for (int j = 0; j < radw; ++j) {
			tmp(i + radh, j) = src(i, radw - j);
			tmp(i + radh, n_cols_new - 1 - j) = src(i, n_cols_old - radw - 1 + j);
		}
	}
	// reflecting top and bottom edges
	for (int j = 0; j < n_cols_old; ++j) {
		for (int i = 0; i < radh; ++i) {
			tmp(i, j + radw) = src(radh - i, j);
			tmp(n_rows_new - 1 - i, j + radw) = src(n_rows_old - radh - 1 + i, j);
		}
	}
	// copying inner part
	for (int i = 0; i < n_rows_old; ++i)
		for (int j = 0; j < n_cols_old; ++j)
			tmp(i + radh, j + radw) = src(i, j);
	return tmp;
}
