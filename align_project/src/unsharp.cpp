#include "unsharp.h"

using std::tuple;
using std::make_tuple;
using std::get;
using std::cout;
using std::string;

const Matrix<double> UnsharpOp::kernel = {{ -1.0/6, -2.0/3, -1.0/6 },
										  { -2.0/3, 13.0/3, -2.0/3 },
										  { -1.0/6, -2.0/3, -1.0/6 }};
const uint UnsharpOp::radius = 1;
const uint UnsharpOp::size = 3;

tuple<uint, uint, uint> UnsharpOp::operator () (const Image &img) const
{
	double sum_r = 0, sum_g = 0, sum_b = 0;

	for (uint i = 0; i < size; ++i) {
		for (uint j = 0; j < size; ++j) {
			sum_r += kernel(i, j) * get<0>(img(i, j));
			sum_g += kernel(i, j) * get<1>(img(i, j));
			sum_b += kernel(i, j) * get<2>(img(i, j));
		}
	}
	sum_r = MAX(0, MIN(255, int(sum_r)));
	sum_g = MAX(0, MIN(255, int(sum_g)));
	sum_b = MAX(0, MIN(255, int(sum_b)));
	return make_tuple(sum_r, sum_g, sum_b);
}

Image unsharp(const Image &src)
{
	if (3 > src.n_rows || 3 > src.n_cols)
		throw string("the kernel is bigger than the image");

	Image tmp = reflect_across_edge(src, 1);
	Image dst = tmp.unary_map(UnsharpOp());

	return dst.submatrix(1, 1, src.n_rows, src.n_cols);
}
