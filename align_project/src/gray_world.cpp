#include "gray_world.h"

using std::tuple;
using std::make_tuple;
using std::tie;
using std::cout;

Image gray_world(const Image &src)
{
	uint n_rows = src.n_rows, n_cols = src.n_cols;
	Image dst(n_rows, n_cols);
	long sum_r = 0, sum_g = 0, sum_b = 0, sum;
	int red, green, blue;
	// The next one is needed for decreasing of the number of operations
	// when one of the channels is completely black
	double one_devided_by_three_n_pixels = 1.0 / 3 / n_cols / n_rows;
	for (uint i = 0; i < n_rows; ++i) {
		for (uint j = 0; j < n_cols; ++j) {
			tie(red, green, blue) = src(i, j);
			sum_r += red;
			sum_g += green;
			sum_b += blue;
		}
	}
	sum = sum_r + sum_g + sum_b;
	for (uint i = 0; i < n_rows; ++i) {
		for (uint j = 0; j < n_cols; ++j) {
			tie(red, green, blue) = src(i, j);
			if (sum_r)
				red = MAX(0, MIN(255, int(red * sum / 3.0 / sum_r)));
			else
				red = MAX(0, MIN(255, int(sum * one_devided_by_three_n_pixels)));
			if (sum_g)
				green = MAX(0, MIN(255, int(green * sum / 3.0 / sum_g)));
			else
				green = MAX(0, MIN(255, int(sum * one_devided_by_three_n_pixels)));
			if (sum_b)
				blue = MAX(0, MIN(255, int(blue * sum / 3.0 / sum_b)));
			else
				blue = MAX(0, MIN(255, int(sum * one_devided_by_three_n_pixels)));
			dst(i, j) = make_tuple(red, green, blue);
		}
	}
	return dst;
}
