#include "align.h"

using std::string;
using std::cout;
using std::tuple;
using std::tie;
using std::get;
using std::make_tuple;

const int nonmax = 4;
const int edge_dist = 10;
const int threshold1 = 100;
const int threshold2 = 250;
const int min_size = 200;
const int limit_start = 20;
const double scale = 1.7;

// Cross Correlation is no good at all.
// Probably because it's not normalized.
// It depends on the size of the interception.
// And the Standart Deviation doesn't.
// Because it's normalized.
// In my humble opinion.
const enum {
	CROSS_CORRELATION = 0,
	STANDART_DEVIATION = 1
} metric_type = STANDART_DEVIATION;

Image align(const Image &src, const string &postprocessing, double fraction)
{
	uint n_rows = src.n_rows, n_cols = src.n_cols;

	uint blue_h  = n_rows / 3;
	uint green_h = n_rows / 3 + (n_rows % 3 / 2) + (n_rows % 3 % 2);
	uint red_h   = n_rows / 3 + (n_rows % 3 / 2);

	int shift_i = 0, shift_j = 0;

	Image edge = canny(src, threshold1, threshold2);

	Image edge_blue  = edge.submatrix(0, 0, blue_h, n_cols);
	Image edge_green = edge.submatrix(blue_h, 0, green_h, n_cols);
	Image edge_red   = edge.submatrix(blue_h + green_h, 0, red_h, n_cols);

	Image blue  = src.submatrix(0, 0, blue_h, n_cols);
	Image green = src.submatrix(blue_h, 0, green_h, n_cols);
	Image red   = src.submatrix(blue_h + green_h, 0, red_h, n_cols);

	Image cut_red   = cut_edge(red  , edge_red  );
	Image cut_green = cut_edge(green, edge_green);
	Image cut_blue  = cut_edge(blue , edge_blue );

	double lim = limit_start;
	tie(shift_i, shift_j) = pyramid(cut_red, cut_green, make_tuple(0, 0), lim, 1);

	Image tmp = merge_red_green(cut_red, cut_green, shift_i, shift_j);

	lim = limit_start;
	tie(shift_i, shift_j) = pyramid(tmp, cut_blue, make_tuple(0, 0), lim, 1);

	Image dst = merge_red_green_blue(tmp, cut_blue, shift_i, shift_j);

	if (postprocessing == "")
		return dst;
	if (postprocessing == "--gray-world")
		return gray_world(dst);
	if (postprocessing == "--unsharp")
		return unsharp(dst);
	if (postprocessing == "--autocontrast")
		return autocontrast(dst, fraction);
	throw string("something's wrong. contact the author of this program");
}

Image cut_edge(const Image &src, const Image &edge)
{
	uint new_edge_up = get_new_edge_up(edge);
	uint new_edge_down = get_new_edge_down(edge);
	uint new_edge_left = get_new_edge_left(edge);
	uint new_edge_right = get_new_edge_right(edge);

	uint span_h = new_edge_down - new_edge_up;
	uint span_w = new_edge_right - new_edge_left;

	return src.submatrix(new_edge_up, new_edge_left, span_h, span_w);
}

uint get_new_edge_up(const Image &edge)
{
	const int n_rows = edge.n_rows, n_cols = edge.n_cols;
	const int near_edge_up = n_rows / edge_dist;

	int sum, max1 = 0, max2 = 0;
	int idx1 = -1, idx2 = -1;

	for (int i = 0; i < near_edge_up; ++i) {
		sum = 0;
		for (int j = 0; j < n_cols; ++j) {
			sum += get<0>(edge(i, j)) / 255;
		}
		if (sum > max1) {
			max1 = sum;
			idx1 = i;
		}
	}
	if (idx1 == -1)
		return 0;
	if (max1 < 2 * edge_dist)
		return 0;
	for (int i = 0; i < near_edge_up; ++i) {
		sum = 0;
		if (ABS(i - idx1) < nonmax)
			continue;
		for (int j = 0; j < n_cols; ++j) {
			sum += get<0>(edge(i, j)) / 255;
		}
		if (sum > max2) {
			max2 = sum;
			idx2 = i;
		}
	}
	return MAX(idx1, idx2);
}

uint get_new_edge_down(const Image &edge)
{
	const int n_rows = edge.n_rows, n_cols = edge.n_cols;
	const int near_edge_down = n_rows - n_rows / edge_dist;

	int sum, max1 = 0, max2 = 0;
	int idx1 = n_rows, idx2 = n_rows;

	for (int i = n_rows - 1; i > near_edge_down; --i) {
		sum = 0;
		for (int j = 0; j < n_cols; ++j) {
			sum += get<0>(edge(i, j)) / 255;
		}
		if (sum > max1) {
			max1 = sum;
			idx1 = i;
		}
	}
	if (idx1 == n_rows)
		return n_rows - 1;
	if (max1 < 2 * edge_dist)
		return n_rows - 1;
	for (int i = n_rows - 1; i > near_edge_down; --i) {
		sum = 0;
		if (ABS(i - idx1) < nonmax)
			continue;
		for (int j = 0; j < n_cols; ++j) {
			sum += get<0>(edge(i, j)) / 255;
		}
		if (sum > max2) {
			max2 = sum;
			idx2 = i;
		}
	}
	return MIN(idx1, idx2);
}

uint get_new_edge_left(const Image &edge)
{
	const int n_rows = edge.n_rows, n_cols = edge.n_cols;
	const int near_edge_left = n_cols / edge_dist;

	int sum, max1 = 0, max2 = 0;
	int idx1 = -1, idx2 = -1;

	for (int j = 0; j < near_edge_left; ++j) {
		sum = 0;
		for (int i = 0; i < n_rows; ++i) {
			sum += get<0>(edge(i, j)) / 255;
		}
		if (sum > max1) {
			max1 = sum;
			idx1 = j;
		}
	}
	if (idx1 == -1)
		return 0;
	if (max1 < 2 * edge_dist)
		return 0;
	for (int j = 0; j < near_edge_left; ++j) {
		sum = 0;
		if (ABS(j - idx1) < nonmax)
			continue;
		for (int i = 0; i < n_rows; ++i) {
			sum += get<0>(edge(i, j)) / 255;
		}
		if (sum > max2) {
			max2 = sum;
			idx2 = j;
		}
	}
	return MAX(idx1, idx2);
}

uint get_new_edge_right(const Image &edge)
{
	const int n_rows = edge.n_rows, n_cols = edge.n_cols;
	const int near_edge_right = n_cols - n_cols / edge_dist;

	int sum, max1 = 0, max2 = 0;
	int idx1 = n_cols, idx2 = n_cols;

	for (int j = n_cols - 1; j > near_edge_right; --j) {
		sum = 0;
		for (int i = 0; i < n_rows; ++i) {
			sum += get<0>(edge(i, j)) / 255;
		}
		if (sum > max1) {
			max1 = sum;
			idx1 = j;
		}
	}
	if (idx1 == n_cols)
		return n_cols - 1;
	if (max1 < 2 * edge_dist)
		return n_cols - 1;
	for (int j = n_cols - 1; j > near_edge_right; --j) {
		sum = 0;
		if (ABS(j - idx1) < nonmax)
			continue;
		for (int i = 0; i < n_rows; ++i) {
			sum += get<0>(edge(i, j)) / 255;
		}
		if (sum > max2) {
			max2 = sum;
			idx2 = j;
		}
	}
	return MIN(idx1, idx2);
}

tuple<int, int> pyramid(const Image &img1, const Image &img2, tuple<int, int> shift0, double &lim, double mult)
{
	tuple<int, int> shift;

	if (img1.n_rows / scale < min_size || img1.n_cols / scale < min_size 
	|| img2.n_rows / scale < min_size || img2.n_cols / scale < min_size) {
		return best_shift(img1, img2, shift0, lim);
	}
	else {
		shift = pyramid(resize(img1, 1 / scale), resize(img2, 1 / scale), shift0, lim /= scale, 2 * mult);
		get<0>(shift) *= scale;
		get<1>(shift) *= scale;
		return best_shift(img1, img2, shift, scale);
	}
}

tuple<int, int> best_shift(const Image &steady, const Image &running, tuple<int, int> &shift0, double limit)
{
	int hs = steady.n_rows, ws = steady.n_cols;
	int hr = running.n_rows, wr = running.n_cols;
	int i0, j0;
	int lim = .5 + limit;

	tie(i0, j0) = shift0;

	int imax = i0, jmax = j0;
	int height = MIN(hs, hr + i0) - MAX(0, i0);
	int width = MIN(ws, wr + j0) - MAX(0, j0);
	int s_row_ofs, s_col_ofs, r_row_ofs, r_col_ofs;

	double max, m;

	do {
// 		Image tmp_s = steady.submatrix(MAX(0, imax), MAX(0, jmax), height, width);
// 		Image tmp_r = running.submatrix(MAX(0, -imax), MAX(0, -jmax), height, width);
		s_row_ofs = MAX(0, imax);
		s_col_ofs = MAX(0, jmax);
		r_row_ofs = MAX(0, -imax);
		r_col_ofs = MAX(0, -jmax);

// 		max = metric(tmp_s, tmp_r);
		max = metric(steady, running, s_row_ofs, s_col_ofs, r_row_ofs, r_col_ofs, height, width);
	} while(0);

	for (int i = -lim; i <= lim; ++i) {
		for (int j = -lim; j <= lim; ++j) {
			if (!i && !j)
				continue;
			height = MIN(hs, hr + i + i0) - MAX(0, i + i0);
			width = MIN(ws, wr + j + j0) - MAX(0, j + j0);
// 			Image tmp_s = steady.submatrix(MAX(0, i + i0), MAX(0, j + j0), height, width);
// 			Image tmp_r = running.submatrix(MAX(0, -(i + i0)), MAX(0, -(j + j0)), height, width);
			s_row_ofs = MAX(0, i + i0);
			s_col_ofs = MAX(0, j + j0);
			r_row_ofs = MAX(0, -(i + i0));
			r_col_ofs = MAX(0, -(j + j0));
// 			m = metric(tmp_s, tmp_r);
			m = metric(steady, running, s_row_ofs, s_col_ofs, r_row_ofs, r_col_ofs, height, width);
			if (m > max) {
				max = m;
				imax = i + i0;
				jmax = j + j0;
			}
		}
	}
	return make_tuple(imax, jmax);
}

// double metric(const Image &img1, const Image &img2)
double metric(const Image &img1, const Image &img2, int row1, int col1, int row2, int col2, int h, int w)
{
	if (metric_type == CROSS_CORRELATION)
		return cross_correlation(img1, img2, row1, col1, row2, col2, h, w);
	if (metric_type == STANDART_DEVIATION)
		return standart_deviation(img1, img2, row1, col1, row2, col2, h, w);
}

// double standart_deviation(const Image &img1, const Image &img2)
double standart_deviation(const Image &img1, const Image &img2, int row1, int col1, int row2, int col2, int h, int w)
{
// 	int n_rows = img1.n_rows, n_cols = img2.n_cols;
	double sum = 0;

// 	for (int i = 0; i < n_rows; ++i) {
// 		for (int j = 0; j < n_cols; ++j) {
	for (int i = 0; i < h; ++i) {
		for (int j = 0; j < w; ++j) {
// 			sum += SQR(get<0>(img1(i, j)) - get<0>(img2(i, j)));
			sum += SQR(get<0>(img1(row1 + i, col1 + j)) - get<0>(img2(row2 + i, col2 + j)));
		}
	}
// 	return (-sum / n_rows / n_cols);
	return (-sum / h / w);
}

// double cross_correlation(const Image &img1, const Image &img2)
double cross_correlation(const Image &img1, const Image &img2, int row1, int col1, int row2, int col2, int h, int w)
{
// 	int n_rows = img1.n_rows, n_cols = img2.n_cols;
	double sum = 0;

// 	for (int i = 0; i < n_rows; ++i) {
// 		for (int j = 0; j < n_cols; ++j) {
	for (int i = 0; i < h; ++i) {
		for (int j = 0; j < w; ++j) {
// 			sum += get<0>(img1(i, j)) * get<0>(img2(i, j));
			sum += get<0>(img1(row1 + i, col1 + j)) * get<0>(img2(row2 + i, col2 + j));
		}
	}
	return sum;
}

Image merge_red_green(const Image &red, const Image &green, int i0, int j0)
{
	int red_h = red.n_rows, red_w = red.n_cols;
	int green_h = green.n_rows, green_w = green.n_cols;
	int n_rows = MIN(red_h, green_h + i0) - MAX(0, i0);
	int n_cols = MIN(red_w, green_w + j0) - MAX(0, j0);
	Image tmp(n_rows, n_cols);
	uint r, g;

	for (int i = 0; i < n_rows; ++i) {
		for (int j = 0; j < n_cols; ++j) {
			r = get<0>(red(MAX(i, i + i0), MAX(j, j + j0)));
			g = get<0>(green(MAX(i, i - i0), MAX(j, j - j0)));
			tmp(i, j) = make_tuple(r, g, 0);
		}
	}
	return tmp;
}

Image merge_red_green_blue(const Image &rg, const Image &blue, int i0, int j0)
{
	int rg_h = rg.n_rows, rg_w = rg.n_cols;
	int blue_h = blue.n_rows, blue_w = blue.n_cols;
	int n_rows = MIN(rg_h, blue_h + i0) - MAX(0, i0);
	int n_cols = MIN(rg_w, blue_w + j0) - MAX(0, j0);
	Image tmp(n_rows, n_cols);
	uint r, g, b;

	for (int i = 0; i < n_rows; ++i) {
		for (int j = 0; j < n_cols; ++j) {
			r = get<0>(rg(MAX(i, i + i0), MAX(j, j + j0)));
			g = get<1>(rg(MAX(i, i + i0), MAX(j, j + j0)));
			b = get<0>(blue(MAX(i, i - i0), MAX(j, j - j0)));
			tmp(i, j) = make_tuple(r, g, b);
		}
	}
	return tmp;
}
