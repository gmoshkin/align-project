#include "canny.h"

using std::cout;
using std::string;
using std::get;
using std::make_tuple;
using std::tie;
using std::tuple;

const double pi8 = .3926990816; // pi/8

Image canny(const Image &src, int threshold1, int threshold2)
{
	Image tmp = gaussian_separable(src, 1.4, 2);
// 	Image gray = gray_scale(tmp);

	Matrix<double> grad_abs(tmp.n_rows, tmp.n_cols);
	Matrix<double> grad_arg(tmp.n_rows, tmp.n_cols);

	gradient(tmp, grad_abs, grad_arg);

	return Image(edge(grad_abs, grad_arg, threshold1, threshold2));
}

Image gray_scale(const Image &img)
{
	const uint n_rows = img.n_rows, n_cols = img.n_cols;
	double red, green, blue, gray;
	Image tmp(n_rows, n_cols);

	for (uint i = 0; i < n_rows; ++i) {
		for(uint j = 0; j < n_cols; ++j) {
			tie(red, green, blue) = img(i, j);
			gray = .299 * red + .587 * green + .114 * blue;
			tmp(i, j) = tie(gray, gray, gray);
		}
	}
	return tmp;
}

void gradient(const Image &src, Matrix<double> &abs, Matrix<double> &arg)
{
	const uint radius = 1;
	const uint size = 3;

	Image img = reflect_across_edge(src, radius);

	const uint n_cols = img.n_cols, n_rows = img.n_rows;

	Matrix<double> tmp_abs(n_rows, n_cols);
	Matrix<double> tmp_arg(n_rows, n_cols);

	int gr_x, gr_y;

	const int start_i = radius;
	const int end_i = n_rows - radius;
	const int start_j = radius;
	const int end_j = n_cols - radius;

	if (size > n_rows || size > n_cols)
		throw string("the kernel is bigger than the image");

	for (int i = start_i; i < end_i; ++i) {
		for (int j = start_j; j < end_j; ++j) {
			gr_x = convol(img, i, j, kernel_x);
			gr_y = convol(img, i, j, kernel_y);
			tmp_abs(i, j) = sqrt(SQR(gr_x) + SQR(gr_y));
			tmp_arg(i, j) = atan2(gr_y, gr_x);
		}
	}
	abs = tmp_abs.submatrix(radius, radius, src.n_rows, src.n_cols);
	arg = tmp_arg.submatrix(radius, radius, src.n_rows, src.n_cols);
}

int convol(const Image &img, int row, int col, const Matrix<double> &kernel)
{
	int sum = 0;

	for (int i = -1; i < 2; ++i) {
		for (int j = -1; j < 2; ++j) {
			// Assuming that the image is gray scale
			sum += kernel(1 + i, 1 + j) * get<0>(img(row + i, col + j));
		}
	}
	return sum;
}

int grad_x(const Image &img)
{
	int sum = 0;

	for (uint i = 0; i < 3; ++i) {
		for (uint j = 0; j < 3; ++j) {
			// Assuming that the image is gray scale
			sum += kernel_x(i, j) * get<0>(img(i, j));
		}
	}
	return sum;
}

int grad_y(const Image &img)
{
	int sum = 0;

	for (uint i = 0; i < 3; ++i) {
		for (uint j = 0; j < 3; ++j) {
			// Assuming that the image is gray scale
			sum += kernel_y(i, j) * get<0>(img(i, j));
		}
	}
	return sum;
}

Image edge(Matrix<double> &grad_abs, Matrix<double> &grad_arg, int t1, int t2)
{
	uint n_rows = grad_abs.n_rows, n_cols = grad_abs.n_cols;
	uint min = (n_rows < n_cols) ? (n_rows) : (n_cols);
	Matrix<int> map(n_rows, n_cols);
	Image res(n_rows, n_cols);
	tuple<uint, uint, uint> zero = make_tuple(0, 0, 0);
	double arg;

	// The edges of the image don't count
	for (uint i = 0; i < min; ++i) {
		map(i, 0) = map(i, n_cols - 1) = map(0, i) = map(n_rows - 1, i) = 0;
		res(i, 0) = res(i, n_cols - 1) = res(0, i) = res(n_rows - 1, i) = zero;
	}
	if (n_rows == min)
		for (uint i = n_rows; i < n_cols; ++i) {
			map(0, i) = map(n_rows - 1, i) = 0;
			res(0, i) = res(n_rows - 1, i) = zero;
		}
	else
		for (uint i = n_cols; i < n_rows; ++i) {
			map(i, 0) = map(i, n_cols - 1) = 0;
			res(i, 0) = res(i, n_cols - 1) = zero;
		}
	// The inner part does
	// The first passage
	for (uint i = 1; i < n_rows - 1; ++i) {
		for (uint j = 1; j < n_cols - 1; ++j) {
			arg = grad_arg(i, j);
			map(i, j) = check_edge_pixel(grad_abs, i, j, arg, t1, t2);
			res(i, j) = zero; // This one just makes the map blank
		}
	}
	// The second passage (unfortunately recursive)
	for (uint i = 1; i < n_rows - 1; ++i) {
		for (uint j = 1; j < n_cols - 1; ++j) {
			if (map(i, j) == 2) {
				map(i, j) = 3; // Means that the pixel has been checked
				res(i, j) = make_tuple(255, 255, 255);
				check_neighbourhood(map, res, i, j);
			}
		}
	}
	return res;
}	

int check_edge_pixel(const Matrix<double> &grad_abs, int row, int col, double arg, int t1, int t2)
{
	double front, back, abs;

	abs = grad_abs(row, col);
	if (abs < t1)
	// Not an edge pixel
		return 0;
	get_neighbours(grad_abs, row, col, arg, front, back);
	if (abs <= front || abs <= back)
	// Nonmaxima
		return 0;
	if (abs <= t2)
	// Weak edge pixel
		return 1;
	// Strong edge pixel
	return 2;
}

void get_neighbours(const Matrix<double> &grad_abs, int row, int col, double arg, double &front, double &back)
{
	if (arg > -pi8 && arg <= pi8) {
		front = grad_abs(row, col + 1);
		back = grad_abs(row, col - 1);
	} else if (arg > pi8 && arg <= 3 * pi8) {
		front = grad_abs(row - 1, col + 1);
		back = grad_abs(row + 1, col - 1);
	} else if (arg > 3 * pi8 && arg <= 5 * pi8) {
		front = grad_abs(row - 1, col);
		back = grad_abs(row + 1, col);
	} else if (arg > 5 * pi8 && arg <= 7 * pi8) {
		front = grad_abs(row - 1, col - 1);
		back = grad_abs(row + 1, col + 1);
	} else if (arg > 7 * pi8 || arg <= -7 * pi8) {
		front = grad_abs(row, col - 1);
		back = grad_abs(row, col + 1);
	} else if (arg > -7 * pi8 && arg <= -5 * pi8) {
		front = grad_abs(row + 1, col - 1);
		back = grad_abs(row - 1, col + 1);
	} else if (arg > -5 * pi8 && arg <= -3 * pi8) {
		front = grad_abs(row + 1, col);
		back = grad_abs(row - 1, col);
	} else if (arg > -3 * pi8 && arg <= -pi8) {
		front = grad_abs(row + 1, col + 1);
		back = grad_abs(row - 1, col - 1);
	}
}

void check_neighbourhood(Matrix<int> &map, Image &res, uint row, uint col)
{
	for (int i = 1; i >= -1; --i) {
		for (int j = 1; j >= -1; --j) {
			if (map(row + i, col + j) != 3 && map(row + i, col + j)) {
				map(row + i, col + j) = 3;
				res(row + i, col + j) = make_tuple(255, 255, 255);
				check_neighbourhood(map, res, row + i, col + j);
			}
		}
	}
}
