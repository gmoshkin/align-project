#include "canny.h"

using std::cout;
using std::string;
using std::get;
using std::make_tuple;
using std::tie;
using std::tuple;

const double pi8 = .3926990816; // pi/8

Image canny(const Image &src, int threshold1, int threshold2)
{
	Image tmp = gaussian_separable(src, 1.4, 2);
	Image gray = gray_scale(tmp);

	Matrix<int> grad_abs(gray.n_rows, gray.n_cols);
	Matrix<double> grad_arg(gray.n_rows, gray.n_cols);

	gradient(gray, grad_abs, grad_arg);

	return Image(edge(grad_abs, grad_arg, threshold1, threshold2));
}

Image gray_scale(const Image &img)
{
	const uint n_rows = img.n_rows, n_cols = img.n_cols;
	double red, green, blue, gray;
	Image tmp(n_rows, n_cols);

	for (uint i = 0; i < n_rows; ++i) {
		for(uint j = 0; j < n_cols; ++j) {
			tie(red, green, blue) = img(i, j);
			gray = .299 * red + .587 * green + .114 * blue;
			tmp(i, j) = tie(gray, gray, gray);
		}
	}
	return tmp;
}

void gradient(const Image &src, Matrix<int> &abs, Matrix<double> &arg)
{
	const uint radius = 1;
	const uint size = 3;

	Image img = reflect_across_edge(src, radius);

	const uint n_cols = img.n_cols, n_rows = img.n_rows;
	Image tmp(n_rows, n_cols);

	Matrix<int> tmp_abs(n_rows, n_cols);
	Matrix<double> tmp_arg(n_rows, n_cols);

	int gr_x, gr_y;

	const uint start_i = radius;
	const uint end_i = n_rows - radius;
	const uint start_j = radius;
	const uint end_j = n_cols - radius;

	if (size > n_rows || size > n_cols)
		throw string("the kernel is bigger than the image");

	for (uint i = start_i; i < end_i; ++i) {
		for (uint j = start_j; j < end_j; ++j) {
			Image neighbourhood = img.submatrix(i - radius, j - radius, size, size);
			gr_x = grad_x(neighbourhood);
			gr_y = grad_y(neighbourhood);
			tmp_abs(i, j) = sqrt(SQR(gr_x) + SQR(gr_y));
			tmp_arg(i, j) = atan2(gr_y, gr_x);
			tmp(i, j) = make_tuple(0, 0, sqrt(SQR(gr_x) + SQR(gr_y)));
		}
	}
	abs = tmp_abs.submatrix(radius, radius, src.n_rows, src.n_cols);
	arg = tmp_arg.submatrix(radius, radius, src.n_rows, src.n_cols);
}

int grad_x(const Image &img)
{
	int sum = 0;

	for (uint i = 0; i < 3; ++i) {
		for (uint j = 0; j < 3; ++j) {
			// Assuming that the image is gray scale
			sum += kernel_x(i, j) * get<0>(img(i, j));
		}
	}
	return sum;
}

int grad_y(const Image &img)
{
	int sum = 0;

	for (uint i = 0; i < 3; ++i) {
		for (uint j = 0; j < 3; ++j) {
			// Assuming that the image is gray scale
			sum += kernel_y(i, j) * get<0>(img(i, j));
		}
	}
	return sum;
}

Image edge(Matrix<int> &grad_abs, Matrix<double> &grad_arg, int t1, int t2)
{
	uint n_rows = grad_abs.n_rows, n_cols = grad_abs.n_cols;
	uint min = (n_rows < n_cols) ? (n_rows) : (n_cols);
	Matrix<int> map(n_rows, n_cols);
	Image res(n_rows, n_cols);
	tuple<uint, uint, uint> zero = make_tuple(0, 0, 0);
	double arg;

	// The edges of the image don't count
	for (uint i = 0; i < min; ++i) {
		map(i, 0) = map(i, n_cols - 1) = map(0, i) = map(n_rows - 1, i) = 0;
		res(i, 0) = res(i, n_cols - 1) = res(0, i) = res(n_rows - 1, i) = zero;
	}
	if (n_rows == min)
		for (uint i = n_rows; i < n_cols; ++i) {
			map(0, i) = map(n_rows - 1, i) = 0;
			res(0, i) = res(n_rows - 1, i) = zero;
		}
	else
		for (uint i = n_cols; i < n_rows; ++i) {
			map(i, 0) = map(i, n_cols - 1) = 0;
			res(i, 0) = res(i, n_cols - 1) = zero;
		}
	// The inner part does
	// The first passage
	for (uint i = 1; i < n_rows - 1; ++i) {
		for (uint j = 1; j < n_cols - 1; ++j) {
			arg = grad_arg(i, j);
			map(i, j) = check_edge_pixel(grad_abs, i, j, arg, t1, t2);
			res(i, j) = zero; // This one just makes the map blank
		}
	}
	// The second passage (unfortunately recursive)
	for (uint i = 1; i < n_rows - 1; ++i) {
		for (uint j = 1; j < n_cols - 1; ++j) {
			if (map(i, j) == 2) {
				map(i, j) = 3; // Means that the pixel has been checked
				res(i, j) = make_tuple(255, 255, 255);
				check_neighbourhood(map, res, i, j);
			}
		}
	}
	return res;
}	

int check_edge_pixel(Matrix<int> &grad_abs, int i, int j, double arg, int t1, int t2)
{
	double front, back;
	int abs = grad_abs(i, j);

	if (abs < t1) {
	// Not an edge pixel
		grad_abs(i, j) = 0;
		return 0;
	}
	get_neighbours(grad_abs.submatrix(i - 1, j - 1, 3, 3), arg, front, back);
	if (abs <= front || abs <= back) {
	// Nonmaxima
		grad_abs(i, j) = 0;
		return 0;
	}
	if (abs < t2)
	// Weak edge pixel
		return 1;
	// Strong edge pixel
	return 2;
}

void get_neighbours(const Matrix<int> &vic, double arg, double &front, double &back)
{
	if (arg > -pi8 && arg <= pi8) {
		front = vic(1, 2);
		back = vic(1, 0);
	} else if (arg > pi8 && arg <= 3 * pi8) {
		front = vic(0, 2);
		back = vic(2, 0);
	} else if (arg > 3 * pi8 && arg <= 5 * pi8) {
		front = vic(0, 1);
		back = vic(2, 1);
	} else if (arg > 5 * pi8 && arg <= 7 * pi8) {
		front = vic(0, 0);
		back = vic(2, 2);
	} else if (arg > 7 * pi8 || arg <= -7 * pi8) {
		front = vic(1, 0);
		back = vic(1, 2);
	} else if (arg > -7 * pi8 && arg <= -5 * pi8) {
		front = vic(2, 0);
		back = vic(0, 2);
	} else if (arg > -5 * pi8 && arg <= -3 * pi8) {
		front = vic(2, 1);
		back = vic(0, 1);
	} else if (arg > -3 * pi8 && arg <= -pi8) {
		front = vic(2, 2);
		back = vic(0, 0);
	}
}

void check_neighbourhood(Matrix<int> &map, Image &res, uint row, uint col)
{
	for (int i = 1; i >= -1; --i) {
		for (int j = 1; j >= -1; --j) {
			if (map(row + i, col + j) != 3 && map(row + i, col + j)) {
				map(row + i, col + j) = 3;
				res(row + i, col + j) = make_tuple(255, 255, 255);
				check_neighbourhood(map, res, row + i, col + j);
			}
		}
	}
}
