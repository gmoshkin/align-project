#pragma once

#include <math.h>
#include <string>
#include "gauss.h"
#include "io.h"
#include "mathmac.h"
#include "matrix.h"
#include "sobel.h"
#include "reflect_across_edge.h"

Image canny(const Image &src, int threshold1, int threshold2);
Image gray_scale(const Image &img);
void gradient(const Image &img, Matrix<int> &abs, Matrix<double> &arg);
int grad_x(const Image &img);
int grad_y(const Image &img);
Image edge(Matrix<int> &grad_abs, Matrix<double> &grad_arg, int t1, int t2);
int check_edge_pixel(Matrix<int> &grad_abs, int i, int j, double arg, int t1, int t2);
void get_neighbours(const Matrix<int> &vic, double arg, double &f, double &b);
void check_neighbourhood(Matrix<int> &map, Image &res, uint row, uint col);
